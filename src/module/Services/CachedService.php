<?php

declare (strict_types = 1);

namespace BlamelessWeb\Caching\Services;

use Nette\Caching\Cache;
use Nette\Caching\Storage;

abstract class CachedService
{
    protected const DEFAULT_CACHE_EXPIRE = '+ 1 day';
    protected const LOCAL_NAMESPACE = self::LOCAL_NAMESPACE;

    protected Cache $cache;

    public function __construct(
        protected Storage $storage
    ) {
        $this->cache = new Cache($storage, $this::LOCAL_NAMESPACE);
    }

    protected function flushCacheByNamespace(string $namespace): void
    {
        $this->flushCacheByNamespaces([$namespace]);
    }

    protected function flushCacheByNamespaces(array $namespaces): void
    {
        $this->cache->clean([
            Cache::Namespaces => $namespaces,
        ]);
    }
}
